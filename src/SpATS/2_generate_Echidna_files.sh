#!/bin/bash

cd ~/projects/spatial_variation/src/SpATS/

TEMPLATE=SpATS_ASReml_template.as

mkdir -p ASReml_AS_files

# Save as an .es file for Echidna!
while read TRAIT
do
  sed "s/TRAIT_NAME/$TRAIT/g" $TEMPLATE > ASReml_AS_files/NAM_${TRAIT}.es
done < ../../data/NAM_trait_names.txt
