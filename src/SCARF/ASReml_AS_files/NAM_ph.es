NAM Data Re-fit from Hung2012
  Genotype !A 7000 !LL 36 !PRUNE
  Family !A 27 !L p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 p18 p19 p20 p21 p22 p23 p24 p25 p26 p27
  Location !A
  Field
  Range *
  Pass *
  asi
  cob_diameter
  cob_length
  cob_mass
  days_to_anthesis
  days_to_silk
  ear_mass
  ear_row_num
  eh
  kernel_number
  kernels_per_row
  leaf_length
  leaf_width
  ph
  tassel_length
  tassel_primary_branch_num
  total_kernel_weight
  twenty_kernel_weight
  upper_leaf_angle

../../../data/SCARF/SCARF.csv !SKIP 1
!CONTINUE !MAXITER 50 !FCON !WORKSPACE 20000 !DENSE 5000 !NODISPLAY

# Mixed model similar to Hung 2012, but without spatial corrections
ph ~ mu,
  !r Loc Fam Loc.Fam at(Fam).Geno at(Fam).Geno.Loc
  residual idv(units)
predict Family Genotype !PRESENT Family Genotype
predict Family
VPREDICT !DEFINE

