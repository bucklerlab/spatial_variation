#!/bin/bash

TRAITS=../../data/NAM_trait_names.txt
DATA=../../data/SCARF/SCARF.csv
RESULTS=../../data/SCARF/echidna_results

mkdir -p $RESULTS

# Make copy of data in dir where output will go
cp $DATA ${RESULTS%%echidna_results}

while read TRAIT
do
  echo "Starting Echidna Analysis of $TRAIT"
  # Make copy of .as file in results directory.  Then by running that copy,
  #  the output will go in the results directory as well.
  RUNAS=${RESULTS}/NAM_${TRAIT}.es
  cp ASReml_AS_files/NAM_${TRAIT}.es $RUNAS
  # Run ASReml
  ES -W12 $RUNAS > ${RESULTS}/NAM_${TRAIT}.log 2>&1
done < $TRAITS
