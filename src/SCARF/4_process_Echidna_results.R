setwd("~/projects/spatial_variation/src/SCARF/")

# Packages
##############
library(tidyverse)
library(magrittr)
source("../generalizedH2FromEchidna.R")

# File paths
##############
trait_name_file  <- "../../data/NAM_trait_names.txt"
# results_dir      <- "/media/jlg374/Data1/spatial_variation/data/NAM-All-Traits-Results/asreml"
# heritability_out <- "/media/jlg374/Data1/spatial_variation/data/NAM_Emre_generalized_heritability.txt"

results_dir      <- "~/projects/spatial_variation/data/SCARF/echidna_results/"
heritability_out <- "../../data/NAM_SCARF_generalized_heritability.txt"

# Main
##############
traits <- read.table(trait_name_file, header=FALSE, stringsAsFactors = FALSE)[,1]

H2 <- data.frame(Trait = traits,
	             # Method = "Emre_Model",
	             Method = "1CV",
	             SED = NA,
	             Var_g = NA,
	             H2 = NA)

for(trait in traits){
	results_path_base <- paste0(results_dir, "/NAM_", trait)
  if(file.exists(paste0(results_path_base, ".esv"))){
  	trait_H2 = generalizedH2FromASReml(results_path_base, "Geno", NAM=TRUE, familyTerm="Fam")
  } else{
    trait_H2 = rep(NA, 3)
  }
  	H2[H2$Trait == trait, 3:5] <- trait_H2
}

write.table(H2, heritability_out, 
	        row.names = FALSE, col.names = TRUE,
	        sep = "\t", quote = FALSE)

